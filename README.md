### Installation
1. Install NVM , to check installed NVM  **` nvm —version`**
2. Install Node V 10.18 using NVM  **`nvm install 10.18`**
3. Set default node version to 10.18 **`nvm alias default 10.18`**
4. Install freshDesk CLI 
        **`npm install https://dl.freshdev.io/cli/fdk.tgz -g`**
5. Check fdk version **`fdk version`**

### command to run app
$fdk run  - to run the app

### Folder structure explained

    .
    ├── README.md                  This file
    ├── app                        Contains the files that are required for the front end component of the app
    │   ├── app.js                 JS to render the dynamic portions of the 
    │   ├── actions.js             Contains all the actions for mappings
    app
    │   ├── icon.svg               Sidebar icon SVG file. Should have a resolution of 64x64px.
    │   ├── freshdesk_logo.png     The Freshdesk logo that is displayed in the app
    │   ├── style.css              Style sheet for the app
    │   ├── template.html          Contains the HTML required for the app’s UI
    ├── config                     Contains the installation parameters and OAuth configuration
    │   ├── iparams.json           Contains the parameters that will be collected during installation
    │   └── iparam_test_data.json  Contains sample Iparam values that will used during testing
    └── manifest.json              Contains app meta data and configuration information

