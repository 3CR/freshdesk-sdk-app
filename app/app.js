
$(document).ready(function () {

  var SurveyMap = [];
  var MappingsEvents = [];
  var MappingsActions = [];
  // var Mappings=[];
  var Mappings = new Map();
  let surMap = new Map();
  var MapCount = 0;
  app.initialized()
    .then(function (_client) {
      var client = _client;

      client.events.on('app.activated',
        function () {
          surveys(client); // for fetching all the surveys from users survey sparrow account
          addNewMap(); // for mapping
          ClickActions(); // all the click actions 
          ConfigMapping(); // configurations after mapping

        }
      );
    });

  function ClickActions() {
    $(document).on('click', '#Devents a', function (con) {
      console.log($(this).parents('.row').attr('id'));
      console.log(con.currentTarget.innerText);
      MappingsEvents[$(this).parents('.row').attr('id')] = con.currentTarget.innerText;
      $(this).parent().parent().children('button').html(con.currentTarget.innerText);
    });

    $(document).on('click', '#Dactions a', function (con) {
      console.log($(this).parents('.row').attr('id'));
      console.log(con.currentTarget.innerText);
      MappingsActions[$(this).parents('.row').attr('id')] = con.currentTarget.innerText;
      $(this).parent().parent().children('button').html(con.currentTarget.innerText);
    });

  }
  function addNewMap() {
    $('#addButton').click(function () {
      $('#mapTable').append(`<div class="row" id=${++MapCount}>
        <div class="dropdown col-xs-6" id="Dactions">
          <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown button
          </button>
          <div class="dropdown-menu "  aria-labelledby="dropdownMenuButton">
            <a class= "dropdown-item " >close</a>
            <a class= "dropdown-item " >delete</a>
            <a class= "dropdown-item " >reply</a>
          </div>
        </div>
        <div class="dropdown col-xs-6" id="Devents">
            <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown button
            </button>
            <div class="dropdown-menu" id="events" aria-labelledby="dropdownMenuButton">
            
            </div>
          </div>
       </div>`);

      for (var x in SurveyMap)
        $(`#${MapCount} #Devents #events`).append(SurveyMap[x]);

    });

  }
  function surveys(client) {
    var surveys = [];

    var AT = 'pr277v2AgnW7OvDjSAtaeSdEQuN4Zq9A8sBBhssat1RHzDK7D2ldb4fjAr4a2AEhoeaA23E9hStSpZYMpIpRhmBw';
    headers = { Authorization: `Bearer ${AT}` },
      reqData = { headers: headers, isOAuth: true },
      url = "https://api.surveysparrow.com/v1/surveys";

    client.request.get(url, reqData)
      .then(function (data) {
        console.log(data);
        var response = JSON.parse(data.response);
        console.log(response);
        response.surveys.map((x) => surveys.push(x.name));

        return response.surveys;

      })
      .then(function (data) {
        var surveysURL = [];
        $('#survey').data('surveyData', data);
        data.map((x) => {
          console.log(x.id);
          var PURL = `https://api.surveysparrow.com/v1/surveys/${x.id}/share/link`;
          client.request.post(PURL, reqData)
            .then(
              function (data) {
                var response = JSON.parse(data.response);
                console.log(response.url);
                surMap[x.name] = response.url;
                surveysURL.push(response.url);
                $('#survey').data('survey', surMap);


              }
            );
        });
        return surveysURL;
      })
      .then(function (data) {
        var i = 0;
        for (var x in surveys)
          SurveyMap.push(`<a class="dropdown-item"  >${surveys[x]}</a> `);

      });
  }

  function ConfigMapping() {

    $(document).on('click', '#btn-success', function (con) {

      for (var x in MappingsEvents)
        // Mappings.push({'action':MappingsActions[x], 'survey': MappingsEvents[x]});
        // Mappings[MappingsActions[x]]= MappingsEvents[x];
        localStorage.setItem(MappingsActions[x], MappingsEvents[x]);
      $('#mapTable').data('mappings', Mappings);
      console.log($('#mapTable').data());

    });



  }


});
