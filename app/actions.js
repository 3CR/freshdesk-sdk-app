
$(document).ready(function () {



  app.initialized()
    .then(function (_client) {
      var client = _client;

      client.events.on("ticket.replyClick", function (event) {

        var surMap = $('#survey').data('survey');

        // for embeding the link of that survey in ticket tab

        console.log(localStorage.getItem('reply'));
        client.interface.trigger("setValue", { id: "editor", text: surMap[localStorage.getItem('close')], replace: false, position: "end" })
          .then(function (data) {
            // data - success message
          }).catch(function (error) {
            // error - error object
          });



        client.interface.trigger("showNotify", {
          type: "success",
          message: {
            title: "Success",
            description: "Your message has been sent"
          },


        });

      });



      client.events.on("ticket.closeTicketClick", function (event) {
        console.log(event.type + " event occurred");


        // for fetching share id from survey id 
        var AT = 'pr277v2AgnW7OvDjSAtaeSdEQuN4Zq9A8sBBhssat1RHzDK7D2ldb4fjAr4a2AEhoeaA23E9hStSpZYMpIpRhmBw';
        headers = { Authorization: `Bearer ${AT}` };
        var surveyData = $('#survey').data('surveyData');
        var shareSurveyId;
        var shareId;
        surveyData.map((x) => {
          if (x.name == localStorage.getItem('close')) {
            shareSurveyId = x.id;
            console.log(shareSurveyId);

            var urlO = `https://api.surveysparrow.com/v1/surveys/${shareSurveyId}/shares`;
            console.log(urlO);
            reqDataO = { headers: headers, isOAuth: true };
            client.request.get(urlO, reqDataO)
              .then(
                function (e, r, data) {

                  if (e.status == 200) {
                    console.log(e);
                    data = e.response;

                    data = JSON.parse(data);
                    data = data.shares;
                    data.map((x) => {
                      if (x.type == 'EMAIL') {
                        console.log(x.type);
                        shareId = x.id;
                        console.log(shareId);
                        return shareId;
                      }
                    });
                  }
                  else {
                    console.log("error fetching share id");
                  }

                })
              .then((si) => {
                console.log(shareId);
                reqData = { headers: headers, isOAuth: true, body: JSON.stringify({ "contacts": ["chaithanyareddy34@gmail.com"] }) };
                url = `https://api.surveysparrow.com/v1/shares/email/${shareId}`;

                //for sending mail using share id
                client.request.put(url, reqData)
                  .then(
                    function (e, r, data) {

                      if (e)
                        console.log(e);
                      else
                        console.log(data);


                    });
              });
          }
        });

      });


      client.events.on("ticket.sendReply", function (event) {
        console.log(event.type + " event occurred");
        client.interface.trigger("showNotify", {
          type: "success",
          message: {
            title: "Success",
            description: "ticket Testing !"
          },
        });
      });

      client.events.on("ticket.statusChanged", function (event) {

        console.log(event.type + " event occurred");
        client.interface.trigger("showNotify", {
          type: "success",
          message: {
            title: "Success",
            description: "statusChanged!"
          },
        });

        // similarly any actions can be added on what to happen when tickt status is changed
        // the survey mapped to statusChanged can be fetched using local storage 
      });
    });
});















